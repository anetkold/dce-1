**Popis zadání a řešení:**

Semestrální práce z předmětu KIV/DCE spočívala ve vytvoření konfigurovatelné struktury libovolného počtu backendů a jednoho loadbalanceru. Využité technologie byli **Terraform** a **Ansible**.

Nejprve se pomocí terraform skriptů vytvoří a nastaví virtuální stroje v Opennebula software (k dispozici na adrese nuada.zcu.cz). Poté terraform vygeneruje konfigurační soubory pro Ansible a nginx, který bude fungovat jako výše zmíněný loadbalancer. Dojde tedy vždy k předání IP adres virtuálních strojů z terraformu do ansible konfigurace, potažmo i do nginx nastavení.

Po skončení terraformu se spustí Ansible v root adresáři projektu, a to následujícím skriptem:

"_ansible-playbook -i ./dynamic\_inventories/cluster demo-cluster.yml"_

Ansible nainstaluje a automaticky nastaví na loadbalancer nginx a na strojích, které mají fungovat jako backend nainstaluje Apache, PHP a nahraje jednoduchý php skript, který po přístupu na IP adresu loadbalancer uzlu zobrazí IP adresu backendu a verzi PHP. Po opětovném načtení téže stránky se IP adresa backendu mění.

Loadbalancer používá standardní metodu pro rozložení zátěže, kterou je Round Robin.

**Postup spuštění:**
Aplikaci spustíme ve VisualStudioCode, kde ji otevřeme v docker kontejneru. Po otevření a spuštění terminálu zadáme následující čtyři příkazy:
1. Terraform init
2. Terraform plan
3. Terraform apply
4. ansible-playbook -i ./dynamic\_inventories/cluster demo-cluster.yml
